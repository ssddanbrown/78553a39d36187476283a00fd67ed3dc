This is a hack to BookStack, using the theme system, to enable auto-sorting of book chapters and pages upon page or chapter create/update.
It sorts by name, ascending, with chapters first.
By default it will run for any book with an `Autosort` tag assigned.

### Setup

This uses the [logical theme system](https://github.com/BookStackApp/BookStack/blob/development/dev/docs/logical-theme-system.md).

1. Within the BookStack install folder, you should have a `themes` folder.
2. Create a `themes/custom/functions.php` file with the contents of the `functions.php` file example below.
3. Customize the tag name, if desired, by teaking the string at around line 45. Set this to empty to run for all books.
4. Add `APP_THEME=custom` to your .env file.

### Note

This can slow down page & chapter create & update system events.
These customizations are not officially supported any may break upon, or conflict with, future updates. 
Quickly tested on BookStack v22.11.1.